window.addEventListener("load", function(){
    var element1 = window.document.querySelector('#gateau'); // grab a reference to your element
    var element2 = window.document.querySelector('#plat'); // grab a reference to your element
    var element3 = window.document.querySelector('#salade'); // grab a reference to your element
    var element4 = window.document.querySelector('#bottle'); // grab a reference to your element
    element1.addEventListener('click', clickHandler); // associate the function above with the click e
    element2.addEventListener('click', clickHandler2); // associate the function above with the click e
    element3.addEventListener('click', clickHandler3); // associate the function above with the click e
    element4.addEventListener('click', clickHandler4); // associate the function above with the click e
},false);


var shown = false;
var shown1 = false;
var shown2 = false;
var shown3 = false;

function clickHandler(){ // declare a function that updates the state
    if(shown === false){
    window.document.querySelector('#texteGateau').style.display = 'block';
    window.document.querySelector('#textePlat').style.display = 'none';
    window.document.querySelector('#texteSalade').style.display = 'none';
    window.document.querySelector('#texteBottle').style.display = 'none';
    shown = true;
    shown1 = false;
    shown2 = false;
    shown3 = false;
    }
    else{
    window.document.querySelector('#texteGateau').style.display = 'none';
    window.document.querySelector('#textePlat').style.display = 'none';
    window.document.querySelector('#texteSalade').style.display = 'none';
    window.document.querySelector('#texteBottle').style.display = 'none';
    shown = false;
    shown1 = false;
    shown2 = false;
    shown3 = false;
    }
}

function clickHandler2(){ // declare a function that updates the state
    if(shown1 === false){
    window.document.querySelector('#texteGateau').style.display = 'none';
    window.document.querySelector('#textePlat').style.display = 'block';
    window.document.querySelector('#texteSalade').style.display = 'none';
    window.document.querySelector('#texteBottle').style.display = 'none';
    shown = false;
    shown1 = true;
    shown2 = false;
    shown3 = false;
    }
    else{
    window.document.querySelector('#texteGateau').style.display = 'none';
    window.document.querySelector('#textePlat').style.display = 'none';
    window.document.querySelector('#texteSalade').style.display = 'none';
    window.document.querySelector('#texteBottle').style.display = 'none';
    shown = false;
    shown1 = false;
    shown2 = false;
    shown3 = false;
    }
}

function clickHandler3(){ // declare a function that updates the state
    if(shown2 === false){
    window.document.querySelector('#texteGateau').style.display = 'none';
    window.document.querySelector('#textePlat').style.display = 'none';
    window.document.querySelector('#texteSalade').style.display = 'block';
    window.document.querySelector('#texteBottle').style.display = 'none';
    shown = false;
    shown1 = false;
    shown2 = true;
    shown3 = false;
    }
    else{
    window.document.querySelector('#texteGateau').style.display = 'none';
    window.document.querySelector('#textePlat').style.display = 'none';
    window.document.querySelector('#texteSalade').style.display = 'none';
    window.document.querySelector('#texteBottle').style.display = 'none';
    shown = false;
    shown1 = false;
    shown2 = false;
    shown3 = false;
    }
}

function clickHandler4(){ // declare a function that updates the state
    if(shown3 === false){
    window.document.querySelector('#texteGateau').style.display = 'none';
    window.document.querySelector('#textePlat').style.display = 'none';
    window.document.querySelector('#texteSalade').style.display = 'none';
    window.document.querySelector('#texteBottle').style.display = 'block';
    shown = false;
    shown1 = false;
    shown2 = false;
    shown3 = true;
    }
    else{
    window.document.querySelector('#texteGateau').style.display = 'none';
    window.document.querySelector('#textePlat').style.display = 'none';
    window.document.querySelector('#texteSalade').style.display = 'none';
    window.document.querySelector('#texteBottle').style.display = 'none';
    shown = false;
    shown1 = false;
    shown2 = false;
    shown3 = false;
    }
}